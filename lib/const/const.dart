import 'package:intl/intl.dart';

const String APIkey = "&apiKey=4c15f54588b64929890135f79029a145";

const String country = "id";

const String topHeadline = "https://newsapi.org/v2/top-headlines?";
const String sources = 'https://newsapi.org/v2/sources?';
const String topHeadlineSource = 'https://newsapi.org/v2/top-headlines?sources=';
const String searchNewsUrl = 'https://newsapi.org/v2/everything?q=';

final DateTime now = DateTime.now();
final DateFormat formatter = DateFormat('yyyy-MM-dd');
final String formatted = formatter.format(now);