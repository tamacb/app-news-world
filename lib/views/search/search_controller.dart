import 'package:get/get.dart';
import 'package:shopx/models/product.dart';
import 'package:shopx/services/remote_services.dart';

class SearchController extends GetxController {
  var isLoading = true.obs;
  var searchList = List<Article>().obs;

  @override
  void onInit() {
    fetchSeacrh();
    super.onInit();
  }

  void fetchSeacrh() async {
    var data = Get.parameters['searchtext'];
    try {
      isLoading(true);
      var details = await RemoteServices.searchNews(data);
      if (details != null) {
        searchList.value = details.articles;
      }
    } finally {
      isLoading(false);
    }
  }
}