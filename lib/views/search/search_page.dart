import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:shopx/views/search/search_controller.dart';
import 'package:get/get.dart';

class SearchPage extends StatelessWidget {
  final SearchController _searchController = Get.put(SearchController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () => Get.offAndToNamed('/'),
                  ),
                  Expanded(
                    child: Text(
                      'NewsX',
                      style: TextStyle(
                          fontFamily: 'avenir',
                          fontSize: 32,
                          fontWeight: FontWeight.w900),
                    ),
                  ),
                  Text(
                    "Seacrh news",
                    style: TextStyle(fontWeight: FontWeight.w300),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Obx(() {
                if (_searchController.isLoading.value)
                  return Center(child: CircularProgressIndicator());
                else
                  return StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: _searchController.searchList.length,
                    crossAxisSpacing: 16,
                    mainAxisSpacing: 16,
                    itemBuilder: (context, index) {
                      return Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    height: 180,
                                    width: double.infinity,
                                    clipBehavior: Clip.antiAlias,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: GestureDetector(
                                      onTap: () => Get.toNamed(
                                          '/readingnewspage?title=${_searchController.searchList[index].title}'
                                              '&source=${_searchController.searchList[index].source.name}'
                                              '&url=${_searchController.searchList[index].url}'
                                              '&image=${_searchController.searchList[index].urlToImage}'
                                              '&content=${_searchController.searchList[index].description}'
                                              '&publish=${_searchController.searchList[index].publishedAt}'),
                                      child: Image.network(
                                        _searchController.searchList[index].urlToImage ??
                                            "http://placehold.jp/57x57.png",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8),
                              Text(
                                _searchController.searchList[index].title,
                                maxLines: 2,
                                style: TextStyle(
                                    fontFamily: 'avenir',
                                    fontWeight: FontWeight.w800),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (index) => StaggeredTile.count(1,1.5),
                  );
              }),
            )
          ],
        ),
      ),
    );
  }
}
