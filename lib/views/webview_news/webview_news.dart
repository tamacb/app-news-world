import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:get/get.dart';

class WebViewNews extends StatefulWidget {
  @override
  _WebViewNewsState createState() => _WebViewNewsState();
}

class _WebViewNewsState extends State<WebViewNews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('read news'),
        ),
        body: WebView(
          initialUrl: Get.parameters['urlwebview'],
          javascriptMode: JavascriptMode.unrestricted,
        ));
  }
}

