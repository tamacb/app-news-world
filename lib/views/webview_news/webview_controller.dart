import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewNewsController extends GetxController {
  var isOpen = true.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    openUrl();
  }

  Future<WebView> openUrl() async {
    try {
      isOpen(false);
      return WebView(
        initialUrl: Get.parameters['urlwebview'],
        javascriptMode: JavascriptMode.unrestricted,
      );
    } catch (e) {
      print(e);
    } finally {}
  }
}
