import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:shopx/views/source/sourcecontrol.dart';


class SourcePage extends StatelessWidget {
  final SourceController _sourceController = Get.put(SourceController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Source Page')),
      body: Obx(
        () => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (_sourceController.sourceList.isEmpty) ? LinearProgressIndicator() : Expanded(
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 3,
                itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.all(5.0),
                  child: GestureDetector(
                    onTap: () => Get.toNamed(
                        '/sourcedetailpage?id=${_sourceController.sourceList[index].id}&'
                        'name=${_sourceController.sourceList[index].name}'),
                    child: Container(
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Center(
                          child:
                              Text(_sourceController.sourceList[index].name)),
                    ),
                  ),
                ),
                staggeredTileBuilder: (index) => StaggeredTile.count(1,1),
                itemCount: _sourceController.sourceList.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
