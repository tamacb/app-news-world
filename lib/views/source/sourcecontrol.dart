import 'package:get/state_manager.dart';
import 'package:shopx/models/source.dart';
import 'package:shopx/services/remote_services.dart';
import 'package:get/get.dart';

class SourceController extends GetxController {
  var isLoading = true.obs;
  var sourceList = List<Source>().obs;
  var detailList = List<Source>().obs;

  @override
  void onInit() {
    fetchSources();
    super.onInit();
  }

  void fetchSources() async {
    try {
      isLoading(true);
      var sources = await RemoteServices.fetchSources();
      if (sources != null) {
        sourceList.value = sources.sources;
      }
    } finally {
      isLoading(false);
    }
  }
}
