import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class ReadPage extends StatelessWidget {
  var dataUrl = Get.parameters['url'];
  var image = Get.parameters['image'];
  var publish = Get.parameters['publish'];
  var title = Get.parameters['title'];
  var source = Get.parameters['source'];
  var content = Get.parameters['content'];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () => Get.back(),
                      ),
                      Expanded(
                        child: Text(
                          'NewsX',
                          style: TextStyle(
                              fontFamily: 'avenir',
                              fontSize: 32,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      Text(
                        "Details news",
                        style: TextStyle(fontWeight: FontWeight.w300),
                      ),
                      IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () => Get.bottomSheet(Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8),
                                topLeft: Radius.circular(8)),
                            color: Colors.white,
                          ),
                          width: double.infinity,
                          height: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: TextField(
                                  decoration: InputDecoration(
                                    labelText: "What's your looking for ?",
                                  ),
                                ),
                                flex: 3,
                              ),
                              Flexible(
                                child: IconButton(
                                  onPressed: () {},
                                  icon: Icon(Icons.search),
                                ),
                                flex: 1,
                              )
                            ],
                          ),
                        )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Container(
                          height: 200.0,
                          width: double.infinity,
                          child: (image == null)
                              ? CircularProgressIndicator()
                              : Image.network(
                                  image,
                                  fit: BoxFit.cover,
                                )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Published At : " + publish.toString(),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Source : " + source,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          dataUrl,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                      Text(
                        content.toString(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        bottomNavigationBar: GestureDetector(
          onTap: ()=> Get.toNamed(
              '/webview?urlwebview=$dataUrl&source=$source'),
          child: Container(
            width: double.infinity,
            height: 50.0,
            color: Colors.blue,
            child: Center(
                child: Text(
              'read more',
              style: TextStyle(fontWeight: FontWeight.w600),
            )),
          ),
        ),
      ),
    );
  }
}
