import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopx/views/source_details_news/source_details_page_controller.dart';


class SourceDetails extends StatelessWidget {
  final DetailsController _detailsController = Get.put(DetailsController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => SafeArea(
          child: Scaffold(
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Center(
                        child: Text(
                      "${Get.parameters["name"]}",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    )),
                    width: double.infinity,
                    height: 100.0,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(15)),
                  ),
                  (_detailsController.detailList.isEmpty) ? LinearProgressIndicator() : ListView.builder(
                    physics: ScrollPhysics(),
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () => Get.toNamed(
                          '/readingnewspage?title=${_detailsController.detailList[index].title}'
                          '&source=${_detailsController.detailList[index].source.name}'
                          '&url=${_detailsController.detailList[index].url}'
                          '&image=${_detailsController.detailList[index].urlToImage}'
                          '&content=${_detailsController.detailList[index].content}'),
                      child: Card(
                        child: Container(
                          width: double.infinity,
                          height: 200.0,
                          child: Column(
                            children: [
                              Image.network(
                                _detailsController
                                        .detailList[index].urlToImage ??
                                    "http://placehold.jp/57x57.png",
                                fit: BoxFit.cover,
                                width: double.infinity,
                                height: 150,
                              ),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                    _detailsController.detailList[index].title, maxLines: 2),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    itemCount: _detailsController.detailList.length,
                    shrinkWrap: true,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
