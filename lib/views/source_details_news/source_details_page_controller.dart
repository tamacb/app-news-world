import 'package:get/state_manager.dart';
import 'package:shopx/models/product.dart';
import 'package:shopx/services/remote_services.dart';
import 'package:get/get.dart';

class DetailsController extends GetxController {
  var isLoading = true.obs;
  var detailList = List<Article>().obs;

  @override
  void onInit() {
    fetchDetails();
    super.onInit();
  }

  void fetchDetails() async {
    var data = Get.parameters['id'];
    try {
      isLoading(true);
      var details = await RemoteServices.fetchDetails(data);
      if (details != null) {
        detailList.value = details.articles;
      }
    } finally {
      isLoading(false);
    }
  }
}
