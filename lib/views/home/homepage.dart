import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:shopx/views/home/homecontroller.dart';

class HomePage extends StatelessWidget {
  final ProductController productController = Get.put(ProductController());
  TextEditingController searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          'NewsX',
                          style: TextStyle(
                              fontFamily: 'avenir',
                              fontSize: 32,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.widgets),
                        onPressed: () => Get.toNamed('source'),
                      ),
                      IconButton(
                        icon: Icon(Icons.person),
                        onPressed: () => Get.toNamed('accountpage'),
                      ),
                      IconButton(
                        icon: Icon(Icons.search),
                        onPressed: () => Get.bottomSheet(Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8),
                                topLeft: Radius.circular(8)),
                            color: Colors.white,
                          ),
                          width: double.infinity,
                          height: 80.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: TextField(
                                  controller: searchController,
                                  decoration: InputDecoration(
                                    labelText: "What's your looking for ?",
                                  ),
                                ),
                                flex: 3,
                              ),
                              Flexible(
                                child: IconButton(
                                  onPressed: () => Get.toNamed(
                                          '/searchnews?searchtext=${searchController.text}')
                                      .whenComplete(
                                          () => searchController.clear()),
                                  icon: Icon(Icons.search),
                                ),
                                flex: 1,
                              )
                            ],
                          ),
                        )),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Obx(() {
                    if (productController.isLoading.value)
                      return Center(child: CircularProgressIndicator());
                    else
                      return StaggeredGridView.countBuilder(
                        crossAxisCount: 2,
                        itemCount: productController.productList.length,
                        crossAxisSpacing: 16,
                        mainAxisSpacing: 16,
                        itemBuilder: (context, index) {
                          return Card(
                            elevation: 2,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Stack(
                                    children: [
                                      Container(
                                        height: 180,
                                        width: double.infinity,
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(4),
                                        ),
                                        child: GestureDetector(
                                          onTap: () => Get.toNamed(
                                              '/readingnewspage?title=${productController.productList[index].title}'
                                              '&source=${productController.productList[index].source.name}'
                                              '&url=${productController.productList[index].url}'
                                              '&image=${productController.productList[index].urlToImage}'
                                              '&content=${productController.productList[index].description}'
                                              '&publish=${productController.productList[index].publishedAt}'),
                                          child: Image.network(
                                            productController.productList[index]
                                                    .urlToImage ??
                                                "http://placehold.jp/57x57.png",
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 8),
                                  Text(
                                    productController.productList[index].title,
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontFamily: 'avenir',
                                        fontWeight: FontWeight.w800),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        staggeredTileBuilder: (index) =>
                            StaggeredTile.count(1, 1.5),
                      );
                  }),
                ),
              ],
            ),
            Positioned(
              bottom: 5,
              right: 10,
              left: 10,
              child: Center(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      FlatButton(
                          onPressed: () => Get.bottomSheet(Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      topLeft: Radius.circular(8)),
                                  color: Colors.white,
                                ),
                                width: double.infinity,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: GestureDetector(
                                            onTap: () => productController
                                                .productList
                                                .sort((a, b) =>
                                                    a.title.compareTo(b.title)),
                                            child: Text('Asccending judul')),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('This Month'),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('This Year'),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              )),
                          child: Row(
                            children: [
                              Text('sort'),
                              Icon(Icons.sort),
                            ],
                          )),
                      SizedBox(
                        child: Container(
                          color: Colors.black,
                          width: 0.5,
                          height: 28,
                        ),
                      ),
                      FlatButton(
                          onPressed: () => Get.bottomSheet(Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(8),
                                      topLeft: Radius.circular(8)),
                                  color: Colors.white,
                                ),
                                width: double.infinity,
                                height: 150.0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Top Source News'),
                                      ),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Indonesian'),
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              )),
                          child: Row(
                            children: [
                              Icon(Icons.filter_list),
                              Text('filter'),
                            ],
                          )),
                    ],
                  ),
                  height: 40.0,
                  width: 180.0,
                  decoration: BoxDecoration(
                      color: Colors.white70,
                      borderRadius: BorderRadius.circular(15)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
