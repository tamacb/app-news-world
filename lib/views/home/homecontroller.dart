import 'package:get/state_manager.dart';
import 'package:shopx/const/const.dart';
import 'package:shopx/models/product.dart';
import 'package:shopx/services/remote_services.dart';
import 'package:get/get.dart';

class ProductController extends GetxController {
  var isLoading = true.obs;
  var isToday = false.obs;
  var productList = List<Article>().obs;

  @override
  void onInit() {
    fetchNews();
    fetchNewsToday();
    super.onInit();
  }

  void fetchNews() async {
    try {
      isLoading(true);
      var news = await RemoteServices.fetchNews();
      if (news != null) {
        productList.value = news.articles;
      }
    } finally {
      isLoading(false);
    }
  }

  void fetchNewsToday() async {
    try {
      isLoading(true);
      var news = await RemoteServices.fetchNewsToday(formatted, formatted);
      if (news != null) {
        productList.value = news.articles;
      }
    } finally {
      isLoading(false);
    }
  }
}
