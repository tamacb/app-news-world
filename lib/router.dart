import 'package:get/get.dart';
import 'package:shopx/views/home/homepage.dart';
import 'package:shopx/views/read_page/read_page.dart';
import 'package:shopx/views/search/search_page.dart';
import 'package:shopx/views/source/sourcepage.dart';
import 'package:shopx/views/source_details_news/source_details_page.dart';
import 'package:shopx/views/webview_news/webview_news.dart';

routes() => [
      GetPage(name: "/", page: () => HomePage()),
      GetPage(name: "/source", page: () => SourcePage()),
      GetPage(name: "/sourcedetailpage", page: () => SourceDetails()),
      GetPage(name: "/readingnewspage", page: () => ReadPage()),
      GetPage(name: "/samplepage", page: () => ReadPage()),
      GetPage(name: "/webview", page: () => WebViewNews()),
      GetPage(name: "/searchnews", page: () => SearchPage()),
    ];
