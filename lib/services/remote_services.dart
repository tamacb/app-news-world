import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shopx/const/const.dart';
import 'package:shopx/models/product.dart';
import 'package:shopx/models/source.dart';

class RemoteServices extends GetxController{
  static var client = http.Client();

  static Future<NewsModel> fetchNews() async {
    var response = await client.get(
        '$topHeadline' + 'country=$country'+'$APIkey');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return newsModelFromJson(jsonString);
    } else {
      print("cek your API or your code !");
      return null;
    }
  }

  static Future<SourceModel> fetchSources() async {
    var response = await client.get(
        '$sources'+'$APIkey');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return sourceModelFromJson(jsonString);
    } else {
      print("cek your API or your code !");
      return null;
    }
  }

  static Future<NewsModel> fetchDetails(String source) async {
    var response = await client.get(
        '$topHeadlineSource'+ '$source' + '$APIkey');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return newsModelFromJson(jsonString);
    } else {
      print("cek your API or your code !");
      return null;
    }
  }

  static Future<NewsModel> searchNews(String search) async {
    var response = await client.get(
        '$searchNewsUrl'+ '$search' + '$APIkey');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return newsModelFromJson(jsonString);
    } else {
      print("cek your API or your code !");
      return null;
    }
  }

  static Future<NewsModel> fetchNewsToday(String from, String to) async {
    var response = await client.get(
        '$topHeadline' + 'country=$country'+'$APIkey'+'from=$from&to=$to&sortBy=popularity');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return newsModelFromJson(jsonString);
    } else {
      print("cek your API or your code !");
      return null;
    }
  }
}
