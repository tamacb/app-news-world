
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopx/router.dart';
import 'package:shopx/views/home/homepage.dart';

Future main() async {
    runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: '/',
      getPages: routes(),
      title: 'NewsX',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
